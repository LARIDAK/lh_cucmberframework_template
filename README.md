Laridak Holding Cucumber Framework Tempalte

This is the test automation project tesmplate for LH it. It supports testing of REST API, Web UI, Mobile UI.

Supported Platforms

Windows
(Mac) OS X
iOS
Linux


Supported Browsers

Google Chrome [Windows, Mac OS X, Linux]
Safari [iOS (real device - iPad, or Simulator), Mac OS X]
Mozilla Firefox [Windows, Mac OS X]
Opera
IE [Windows]
Edge [Windows]