package dataProviders;

import enums.DriversList;
import enums.EnvironmentList;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {

    private Properties properties;
    private String propertyFilePath = "configs/Configuration.properties";

    public void set_propertyFilePath(String propertyFilePath) {
        this.propertyFilePath = propertyFilePath;
    }

    public ConfigFileReader() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }


    public String getChromeDriverPath() {

        String driverPath = properties.getProperty("chromeDriverPath");
        if (driverPath != null) return driverPath;
        else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");

    }

    public String getFFDriverPath() {

        String driverPath = properties.getProperty("firefoxDriverPath");
        if (driverPath != null) return driverPath;
        else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");

    }

    public String getIEDriverPath() {

        String driverPath = properties.getProperty("iEDriverPath");
        if (driverPath != null) return driverPath;
        else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");

    }

    public String getSafariDriverPath() {

        String driverPath = properties.getProperty("safariDriverPath");
        if (driverPath != null) return driverPath;
        else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");

    }

    public long getImplicitlyWait() {

        String implicitlyWait = properties.getProperty("implicitlyWait");
        if (implicitlyWait != null) return Long.parseLong(implicitlyWait);
        else throw new RuntimeException("implicitlyWait not specified in the Configuration.properties file.");

    }

    public String getApplicationUrl() {

        String url = properties.getProperty("kid_url");
        if (url != null) return url;
        else throw new RuntimeException("url not specified in the Configuration.properties file.");

    }

    public String getMiKidApplicationUrl() {

        String url = properties.getProperty("mi_kid_url");
        if (url != null) return url;
        else throw new RuntimeException("url not specified in the Configuration.properties file.");

    }


    public String getApplicationMiUrl() {

        String url = properties.getProperty("mi_url");
        if (url != null) return url;
        else throw new RuntimeException("url not specified in the Configuration.properties file.");

    }

    //    Default value is returned as DriverType.Chrome in case of Null.
    //    Exception is thrown if the value does not match with anything.
    public DriversList getBrowser() {

        String browserName = properties.getProperty("browser");

        if (browserName == null || browserName.equalsIgnoreCase("chrome")) return DriversList.CHROME;
        else if (browserName.equalsIgnoreCase("firefox")) return DriversList.FIREFOX;
        else if (browserName.equalsIgnoreCase("iexplorer")) return DriversList.INTERNETEXPLORER;
        else if (browserName.equalsIgnoreCase("edge")) return DriversList.EDGE;
        else if (browserName.equalsIgnoreCase("safari")) return DriversList.SAFARI;
        else
            throw new RuntimeException("Browser Name Key value in Configuration.properties is not matched : " + browserName);
    }

    //    Local is returned in case of Null and if the value  is equal to Local.
    //    Which means that in case of missing environment property, execution will be carried on local machine.
    public EnvironmentList getEnvironment() {

        String environmentName = properties.getProperty("environment");

        if (environmentName == null || environmentName.equalsIgnoreCase("local")) return EnvironmentList.LOCAL;
        else if (environmentName.equalsIgnoreCase("remote")) return EnvironmentList.WEBREMOTE;
        else
            throw new RuntimeException("Environment Type Key value in Configuration.properties is not matched : " + environmentName);
    }

//    Retrieve the property using getProperty method of Properties class.
//    Null check is performed and in case of null by default value is returned as true.
//    In case of not null, String value is parsed to Boolean.

    public Boolean getBrowserWindowSize() {

        String windowSize = properties.getProperty("windowMaximize");

        if (windowSize != null) return Boolean.valueOf(windowSize);
        return true;
    }

    public String getTestDataResourcePath() {
        String testDataResourcePath = properties.getProperty("testDataResourcePath");
        if (testDataResourcePath != null) return testDataResourcePath;
        else
            throw new RuntimeException("Test Data Resource Path not specified in the Configuration.properties file for the Key:testDataResourcePath");
    }

    public String getReportConfigPath() {
        String reportConfigPath = properties.getProperty("reportConfigPath");
        if (reportConfigPath != null) return reportConfigPath;
        else
            throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");
    }

    public String getChromeEnvironment() {
        String reportConfigPath = properties.getProperty("chrome_Environment");
        if (reportConfigPath != null) return reportConfigPath;
        else
            throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:chrome_Environment");

    }

    public String getFFEnvironment() {
        String reportConfigPath = properties.getProperty("fireFox_Environment");
        if (reportConfigPath != null) return reportConfigPath;
        else
            throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:fireFox_Environment");

    }

    public String getIEEnvironment() {
        String reportConfigPath = properties.getProperty("iE_Environment");
        if (reportConfigPath != null) return reportConfigPath;
        else
            throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:iE_Environment");

    }

    public String getEdgeEnvironment() {
        String reportConfigPath = properties.getProperty("edge_Environment");
        if (reportConfigPath != null) return reportConfigPath;
        else
            throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:edge_Environment");

    }

    public String getSafariEnvironment() {
        String reportConfigPath = properties.getProperty("safari_Environment");
        if (reportConfigPath != null) return reportConfigPath;
        else
            throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:safari_Environment");

    }
}