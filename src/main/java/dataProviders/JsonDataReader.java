package dataProviders;

import gherkin.deps.com.google.gson.Gson;
import managers.ConfigReaderManager;
import testDataTypes.TC_AdmContentMngNewFolder;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class JsonDataReader {

    private final String dataFilePath = ConfigReaderManager.getInstance().getConfigReader().getTestDataResourcePath() ;

    private static List<TC_AdmContentMngNewFolder> tc_admContentMngNewFolders;

    public JsonDataReader() {
        tc_admContentMngNewFolders = getTc_admContentMngNewFolders();
    }


    private List<TC_AdmContentMngNewFolder> getTc_admContentMngNewFolders() {
        Gson gson = new Gson();
        BufferedReader bufferReader = null;
        try {
            bufferReader = new BufferedReader(new FileReader(dataFilePath+"AdministrationFeaturesData/TC_AdmContentMngNewFolder.json"));
            TC_AdmContentMngNewFolder[] tc_admContentMngNewFolders = gson.fromJson(bufferReader, TC_AdmContentMngNewFolder[].class);
            return Arrays.asList(tc_admContentMngNewFolders);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Json file not found at path : " + dataFilePath+"AdministrationFeaturesData/TC_AdmContentMngNewFolder.json");
        } finally {
            try {
                if (bufferReader != null) bufferReader.close();
            } catch (IOException ignore) {
            }
        }
    }

    public static final TC_AdmContentMngNewFolder getTC_AdmContentMngNewFolderByID(String dataID) {
        return tc_admContentMngNewFolders.stream().filter(x-> x.getDATA_ID().equalsIgnoreCase(dataID)).findAny().get();
    }

}
