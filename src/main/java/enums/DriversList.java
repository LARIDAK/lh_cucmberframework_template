package enums;

public enum DriversList {

    FIREFOX,
    CHROME,
    INTERNETEXPLORER,
    EDGE,
    SAFARI
}
