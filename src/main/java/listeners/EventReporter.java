package listeners;

import com.aventstack.extentreports.Status;
import managers.DriverManager;
import managers.ReportManager;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.*;

import java.io.IOException;

public class EventReporter implements ITestListener, ISuiteListener, IInvokedMethodListener {

    /**
     * Method gets invoked once before the start of suite execution.
     */
    public void onStart(ISuite suite) {
    }

    /**
     * Method gets invoked once after the completion of suite execution.
     */
    public void onFinish(ISuite suite) {
    }

    /**
     * Method gets invoked once before start of test execution.
     */
    public void onStart(ITestContext context) {
        Reporter.log("===<>===<>===<>===<>===<>===<>===<>===<>===<>===<>===<>===<>=", true);
    }

    /**
     * Method gets invoked once after completion of test execution.
     */
    public void onFinish(ITestContext context) {
        Reporter.log("===<>===<>===<>===<>===<>===<>===<>===<>===<>===<>===<>===<>=", true);
    }

    /**
     * Method gets invoked once before execution of each test method.
     */
    public void onTestStart(ITestResult result) {
        Reporter.log("=============================================================", true);
        Reporter.log("Executing test method: " + result.getMethod().getMethodName(), true);
    }

    /**
     * Method gets invoked once after every passed test method.
     */
    public void onTestSuccess(ITestResult result) {
        printTestResult(result);
        Reporter.log("=============================================================", true);
    }

    /**
     * Method gets invoked once after every failed test method.
     */
    public void onTestFailure(ITestResult result) {
        printTestResult(result);
        Reporter.log("=============================================================", true);

        //Get driver from BaseTest and assign to local webdriver variable.
        Object testClass = result.getInstance();
        WebDriver webDriver = ((DriverManager) testClass).getDriver();

        //Take base64Screenshot screenshot.
        String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)webDriver).
                getScreenshotAs(OutputType.BASE64);

        //Extentreports log and screenshot operations for failed tests.
        ReportManager.log(Status.FAIL,"Test Failed");
        try {
            ReportManager.getTest().addScreenCaptureFromPath((base64Screenshot));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Method gets invoked once after every skipped test method.
     */
    public void onTestSkipped(ITestResult result) {
        printTestResult(result);
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }

    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
    }

    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
    }

    private void printTestResult(ITestResult result) {
        String status = null;
        switch (result.getStatus()) {
            case ITestResult.SUCCESS:
                status = "Pass";
                break;
            case ITestResult.FAILURE:
                status = "Failed";
                break;
            case ITestResult.SKIP:
                status = "Skipped";
        }
        if ("Pass".equalsIgnoreCase(status)) {
            final String message = "Test Status : " + status.toUpperCase();
            Reporter.log(message, true);
        } else if ("Failed".equalsIgnoreCase(status)) {
            final String message = "Test Status : " + status.toUpperCase();
            Reporter.log(message, true);
        } else if ("Skipped".equalsIgnoreCase(status)) {
            final String message = "Test Status : " + status.toUpperCase();
            Reporter.log(message, true);
        }
    }
}
