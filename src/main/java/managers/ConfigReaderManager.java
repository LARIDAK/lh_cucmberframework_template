
package managers;


import dataProviders.ConfigFileReader;
import dataProviders.JsonDataReader;

public class ConfigReaderManager {

//    ************************* Singleton Design Pattern  *******************************************
//    We use singleton to class to control object creation, limiting the number of objects to only one.
//    Since there is only one Singleton instance, any instance fields of a Singleton will occur only once per class,
//    just like static fields.

    private static ConfigReaderManager fileReaderManager = new ConfigReaderManager();
    private static ConfigFileReader configFileReader;
    private static JsonDataReader jsonDataReader;

    private ConfigReaderManager() {
    }

    public static ConfigReaderManager getInstance() {
        return fileReaderManager;
    }

    public ConfigFileReader getConfigReader() {
        return (configFileReader == null) ? new ConfigFileReader() : configFileReader;
    }

    public JsonDataReader getJsonReader() {
        return (jsonDataReader == null) ? new JsonDataReader() : jsonDataReader;
    }
}