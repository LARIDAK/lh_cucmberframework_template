package managers;

import enums.DriversList;
import enums.EnvironmentList;
import org.ini4j.Wini;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import pageObjects.AuthenticatePage;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.remote.BrowserType.*;

public class DriverManager {

    private WebDriver driver;
    private static DriversList driverType;
    private static EnvironmentList environmentType;
    private static String env = null;

    private String desiredCapabilityFilePath = "configs";
    private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
    private static final String FF_DRIVER_PROPERTY = "webdriver.gecko.driver";
    private static final String IE_DRIVER_PROPERTY = "webdriver.ie.driver";
    private static final String EDGE_DRIVER_PROPERTY = "webdriver.edge.driver";
    private static final String SAFARI_DRIVER_PROPERTY = "webdriver.safari.driver";

    public static java.lang.String username = "test1%40kadrige.com";
    public static java.lang.String authkey = "u77b2febd1853966"; // Your authkey
    CBTManager cbt = new CBTManager(username,authkey);

    public DriverManager(DriversList tag) {
        //get driver type from config file "browser" @Before (Chrome),@Before (FF),...
        driverType = tag;

        //get environment from config file "environment"
        environmentType = ConfigReaderManager.getInstance().getConfigReader().getEnvironment();

    }

    public WebDriver getDriver() {
        if (driver == null) driver = createDriver();
        return driver;
    }

    private WebDriver createDriver() {
        switch (environmentType) {
            case LOCAL:
                driver = createLocalDriver();
                break;
            case WEBREMOTE:
                driver = createRemoteDriver();
                break;
        }
        return driver;
    }

    /**
     * @return
     */
    private WebDriver createRemoteDriver() {

        try {
            driver = new RemoteWebDriver(cbt.getHubUrl(), getDesiredCapabilities(driverType));

            return setWindowSizeAndImplicitWait();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

//        throw new RuntimeException("RemoteWebDriver is not yet implemented");
    }

    private WebDriver createLocalDriver() {
        switch (driverType) {

            case FIREFOX:
                System.setProperty(FF_DRIVER_PROPERTY, ConfigReaderManager.getInstance().getConfigReader().getFFDriverPath());
                driver = new FirefoxDriver();
                break;

            case CHROME:
                System.setProperty(CHROME_DRIVER_PROPERTY, ConfigReaderManager.getInstance().getConfigReader().getChromeDriverPath());
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("useAutomationExtension", false);
                driver = new ChromeDriver(options);
                break;

            case INTERNETEXPLORER:
                System.setProperty(IE_DRIVER_PROPERTY, ConfigReaderManager.getInstance().getConfigReader().getIEDriverPath());
                driver = new InternetExplorerDriver();
                break;

            case EDGE:
                System.setProperty(EDGE_DRIVER_PROPERTY, ConfigReaderManager.getInstance().getConfigReader().getIEDriverPath());
                driver = new EdgeDriver();
                break;

            case SAFARI:
                System.setProperty(SAFARI_DRIVER_PROPERTY, ConfigReaderManager.getInstance().getConfigReader().getSafariDriverPath());
                driver = new SafariDriver();
                break;
        }

        return setWindowSizeAndImplicitWait();
    }

    public void closeDriver() {

        driver.quit();
    }

    public DesiredCapabilities getDesiredCapabilities(DriversList browser) {
        Wini iniFile = null;
        String fileIni;
        switch (browser) {

            case FIREFOX:
                fileIni = "crossFireFoxEnvironment.ini";
                break;
            case CHROME:
                fileIni = "crossChromeEnvironment.ini";
                break;
            case SAFARI:
                fileIni = "crossSafariEnvironment.ini";
                break;
            case INTERNETEXPLORER:
                fileIni = "crossIEEnvironment.ini";
                break;
            case EDGE:
                fileIni = "crossEdgeEnvironment.ini";
                break;
            default:
                fileIni = "crossFireFoxEnvironment.ini";
                break;

        }

        try {
            iniFile = new Wini(new File(desiredCapabilityFilePath + "/" + fileIni));
        } catch (IOException e) {
            System.out.println("File " + desiredCapabilityFilePath + "/" + fileIni +
                    ".ini not found");
        }
        // Récupération de l'argument env
        try {

            env = System.getProperty("env");
            // Si non trouvé, prendre la configuration de base config.ini
            if (env == null) {
                env = iniFile.get("environment", "name");
            }

            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("name", browser);
            caps.setCapability("build", "1.0");
            caps.setCapability("browserName", iniFile.get(env, "browserName"));
            caps.setCapability("record_video", iniFile.get(env, "record_video"));
            caps.setCapability("record_network", iniFile.get(env, "record_network"));
            caps.setCapability("deviceName", iniFile.get(env, "deviceName"));
            caps.setCapability("platformVersion", iniFile.get(env, "platformVersion"));
            caps.setCapability("platform", iniFile.get(env, "platform"));
            caps.setCapability("screenResolution", iniFile.get(env, "screenResolution"));
            caps.setCapability("deviceOrientation", iniFile.get(env, "deviceOrientation"));
            return caps;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private WebDriver setWindowSizeAndImplicitWait() {
        if (ConfigReaderManager.getInstance().getConfigReader().getBrowserWindowSize())
            driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(ConfigReaderManager.getInstance().getConfigReader().getImplicitlyWait(), TimeUnit.SECONDS);

        return driver;
    }
}
