package managers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pageObjects.AuthenticatePage;

public class PageObjectManager {


    private AuthenticatePage authenticatePage;



    private WebDriver driver;

    {
//        PageFactory.initElements(driver, HomePage.class);
    }
    public PageObjectManager(WebDriver driver) {
        this.driver = driver;

    }

//    ****************** Warning this method <get_NameOfPage> well:
//    Create an Object of Page Class only if the object is null.
//    Supply the already created object if it is not null

    public AuthenticatePage getAuthenticatePage() {
        return (authenticatePage == null) ? authenticatePage = new AuthenticatePage(driver) : authenticatePage;
    }

}
