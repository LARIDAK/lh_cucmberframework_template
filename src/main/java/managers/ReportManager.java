package managers;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import java.util.HashMap;
import java.util.Map;

public class ReportManager {

    static Map extentTestMap = new HashMap();
    private static ExtentReports extent;
    private static ExtentTest test;
    private static ExtentHtmlReporter htmlReporter;
    private static String reportName;


    public ReportManager(String reportName){
        this.reportName = reportName;
        extent = GetExtent();
    }
    /**
     * creer une instance de fichier html.
     *
     * @return une instance d'fichier html.
     */
    public static ExtentReports GetExtent( ){
        if (extent != null)
            return extent; //avoid creating new instance of html file
        extent = new ExtentReports();
        extent.attachReporter(getHtmlReporter());
        return extent;
    }
    public static synchronized ExtentTest getTest() {
        return (ExtentTest)extentTestMap.get((int) (long) (Thread.currentThread().getId()));
    }
    /**
     * creer un fichier html.
     *
     * @return un fichier html.
     */
    private static ExtentHtmlReporter getHtmlReporter() {

        htmlReporter = new ExtentHtmlReporter("./reports/"+reportName+".html");

        // make the charts visible on report open
        htmlReporter.config().setChartVisibilityOnOpen(true);

        htmlReporter.config().setDocumentTitle("IQVia KID Automation Report");
        htmlReporter.config().setReportName("Regression cycle");

        return htmlReporter;
    }


    /**
     * creer un test.
     *
     * @param name string
     *
     * @param description string
     *
     * @return retourne l'etat du test.
     */
    public static ExtentTest createTest(String name, String description){
        test = extent.createTest(name, description);
        return test;
    }

    /**
     * log.
     *
     * @param status status
     *
     * @param details string
     *
     * @return void.
     */
    public static void log(Status status, String details){
        test.log(status,details);
    }

    public static void flush(){
        extent.flush();
    }

}

