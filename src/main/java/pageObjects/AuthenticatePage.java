package pageObjects;

import gherkin.lexer.Th;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import seleniumRobot.SeleniumBase;

import java.util.List;

public class AuthenticatePage {
    //
    private WebDriver driver = null;
    private WebDriver driver3 = null;

    //    This is a constructor, to initialize driver for this page
    public AuthenticatePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    /************** Exemples : to use @FindBy ************************* /
     //    @FindBy(id = "Element Id")
     //    private WebElement Element_Name;

     //    @FindBy(css = "Element CSS")
     //    private WebElement Element_Name;

     //    @FindBy(xpath = "Element Xpath")
     //    private WebElement Element_Name;
     / ******************************************************************/


    //    ************************* @CacheLookup :  If we know that element is always present on the page
    //    Healthcare professional

    @FindBy(id = "selected-country")
    public WebElement weSelectCountry;

    @FindBy(xpath = "//*[@id=\"hcp-container\"]/div/span[2]")
    public WebElement weBtnParticipantContainer;

    @FindBy(id = "name")
    public WebElement weTextNameParticipant;

    @FindBy(id = "id")
    public WebElement weTextPasswordParticipant;

    @FindBy(id = "hcp-submit")
    public WebElement weBtnSubmitParticipant;

    @FindBy(xpath = "//*[@id=\"hcp-container\"]/form/div[1]")
    public WebElement weErrorMsgLoginParticipant;

    //    Presenter
    @FindBy(id = "presenter-container")
    public WebElement weBtnPresenterContainer;

    @FindBy(id = "username")
    public WebElement weTextNamePresenter;

    @FindBy(id = "password")
    public WebElement weTextPasswordPresenter;

    @FindBy(id = "presenter-submit")
    @CacheLookup
    public WebElement weBtnSubmitPresenter;

    @FindBy(xpath = "//*[@id='presenter-container']/form/div[1]")
    public WebElement weErrorMsgLoginPresenter;

    @FindBy(id = "lost-password")
    public WebElement weLinkLostPassword;

    //    Lost passowrd popup
    @FindBy(xpath = "//*[@id=\"participant\"]/option[2]")
    public WebElement secondParticipantInTheList;

    @FindBy(id = "pwd_lost")
    public WebElement wePwdLost;

    @FindBy(css = "#pwd_lost > div.buttons > input.cancel")
    public WebElement weBtnCancelLostPwdPopup;

    @FindBy(id = "sendMail")
    public WebElement weBtnSendLostPwdPopup;


    //    General Authenticate methods
    public String participantAuthenticate(String login, String pwd) {
        try {

            return authenticateUser(login, pwd, weBtnParticipantContainer,
                    weTextNameParticipant, weTextPasswordParticipant,
                    weBtnSubmitParticipant, weErrorMsgLoginParticipant);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    public String presenterAuthenticate(String login, String pwd) {
        try {

            return authenticateUser(login, pwd, weBtnPresenterContainer,
                    weTextNamePresenter, weTextPasswordPresenter,
                    weBtnSubmitPresenter, weErrorMsgLoginPresenter);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public boolean participantAuthenticateToken(String token) {
        try {
            SeleniumBase.set_Field(weTextPasswordParticipant, token);
            weBtnSubmitParticipant.click();
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public void selectCountry(WebDriver driver, String name) {

        List<WebElement> menuList = driver.findElements(By.xpath("//div[@id=\"country-container\"]/ul/li"));
        weSelectCountry.click();
        for (int i = 0; i < menuList.size(); i++) {
            if (menuList.get(i).getAttribute("onclick").contains(name)) {
                System.out.println("Selected language " + menuList.get(i).getText());
                menuList.get(i).click();
                break;
            }
        }
    }

    private String authenticateUser(String login, String pwd, WebElement weBtnParticipantContainer, WebElement weTextNameParticipant, WebElement weTextPasswordParticipant, WebElement weBtnSubmitParticipant, WebElement weErrorMsgLoginParticipant) {
        weBtnParticipantContainer.click();
        SeleniumBase.set_Field(weTextNameParticipant, login);
        SeleniumBase.set_Field(weTextPasswordParticipant, pwd);
        weBtnSubmitParticipant.click();
        if (weErrorMsgLoginParticipant.isDisplayed())
            return weErrorMsgLoginParticipant.getText();
        else
            return "";
    }

}





