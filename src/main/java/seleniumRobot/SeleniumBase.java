package seleniumRobot;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static managers.DriverManager.authkey;
import static managers.DriverManager.username;

public class SeleniumBase {
    public static String chemin= System.getProperty("user.dir");
    public static DesiredCapabilities caps;

    public SeleniumBase(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }


    public static void set_Field(WebElement element, String value) {
        element.clear();
        element.sendKeys(value);
    }

    public static WebElement getElementByLink(WebDriver driver, String link){
        return driver.findElement(By.partialLinkText(link));
    }

    public static WebElement getElementByXpath(WebDriver driver, String Xpath){
        return driver.findElement(By.xpath(Xpath));
    }

    public static WebElement getElementByCss(WebDriver driver, String Css){
        return driver.findElement(By.cssSelector(Css));
    }
    public static void switchToFrame(WebDriver driver, String frameName){
        driver.switchTo().frame(frameName);
    }

    public static boolean navigateToUrl(WebDriver driver, String url) {
        try {
            driver.get(url);
            return true;

        } catch (Exception e) {

            return false;
        }

    }


    public static boolean isElementPresent(WebDriver driver,By elementLocator) {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean returnVal = true;
        try{
            driver.findElement(elementLocator).isDisplayed();
        } catch (NoSuchElementException e){
            returnVal = false;
        } finally {
            driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
        }
        return returnVal;
    }

    public static boolean isElementPresent(WebDriver driver,WebElement element) {
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean returnVal = true;
        try{
            element.isDisplayed();
        } catch (NoSuchElementException e){
            returnVal = false;
        } finally {
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }
        return returnVal;
    }

    public static int get_CountOfElementsByXpath(WebDriver driver,String xPath_element) {
        return driver.findElements(By.xpath(xPath_element)).size();
    }

    public static int get_CountOfElementsByCss(WebDriver driver,String css_element) {
        return driver.findElements(By.cssSelector(css_element)).size();
    }

    public static JsonNode setScore(WebDriver driver, String score) throws UnirestException {
        if (driver instanceof RemoteWebDriver) {
            String seleniumTestId = ((RemoteWebDriver) driver).getSessionId().toString();
            // Mark a Selenium test as Pass/Fail
            HttpResponse<JsonNode> response = Unirest.put("http://crossbrowsertesting.com/api/v3/selenium/{seleniumTestId}")
                    .basicAuth(username, authkey).routeParam("seleniumTestId", seleniumTestId).field("action", "set_score")
                    .field("score", score).asJson();
            return response.getBody();
        } else {
            return null;
        }
    }

    public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws IOException {
        //Convert web driver object to TakeScreenshot
        TakesScreenshot scrShot =((TakesScreenshot)webdriver);
        //Call getScreenshotAs method to create image file
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        //Move image file to new destination
        File DestFile=new File(fileWithPath);
        //Copy file at destination
        FileUtils.copyFile(SrcFile, DestFile);

    }

    public static Alert waitForAlert(WebDriver driver, long timeoutSec) throws InterruptedException {
        Alert alert = null;
        WebDriverWait wait = new WebDriverWait(driver, timeoutSec);
        try {
            wait.until(ExpectedConditions.alertIsPresent());
        } catch (UnhandledAlertException f) {
            try {
                alert = driver.switchTo().alert();
                String alertText = alert.getText();
                System.out.println("Alert data: " + alertText);
                // alert.accept();
            } catch (NoAlertPresentException e) {
                e.printStackTrace();
            }
        }
        return  alert;
    }


}
