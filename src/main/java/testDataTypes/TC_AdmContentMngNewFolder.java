package testDataTypes;

public class TC_AdmContentMngNewFolder {
    private String DATA_ID;
    private String FOLDERNAME;
    private String SHARE;
    private int CAMPAIGNINDEX;
    private String NEWCAMPAIGN;
    private int TARGETINDEX;
    private String NEWTARGET;


    // Getter Methods

    public String getDATA_ID() {
        return DATA_ID;
    }

    public String getFOLDERNAME() {
        return FOLDERNAME;
    }

    public String getSHARE() {
        return SHARE;
    }

    public int getCAMPAIGNINDEX() {
        return CAMPAIGNINDEX;
    }

    public String getNEWCAMPAIGN() {
        return NEWCAMPAIGN;
    }

    public int getTARGETINDEX() {
        return TARGETINDEX;
    }

    public String getNEWTARGET() {
        return NEWTARGET;
    }

    // Setter Methods

    public void setDATA_ID(String DATA_ID) {
        this.DATA_ID = DATA_ID;
    }

    public void setFOLDERNAME(String FOLDERNAME) {
        this.FOLDERNAME = FOLDERNAME;
    }

    public void setSHARE(String SHARE) {
        this.SHARE = SHARE;
    }

    public void setCAMPAIGNINDEX(int CAMPAIGNINDEX) {
        this.CAMPAIGNINDEX = CAMPAIGNINDEX;
    }

    public void setNEWCAMPAIGN(String NEWCAMPAIGN) {
        this.NEWCAMPAIGN = NEWCAMPAIGN;
    }

    public void setTARGETINDEX(int TARGETINDEX) {
        this.TARGETINDEX = TARGETINDEX;
    }

    public void setNEWTARGET(String NEWTARGET) {
        this.NEWTARGET = NEWTARGET;
    }
}
