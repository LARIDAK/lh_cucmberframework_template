package cucumberContext;

import enums.Context;
import enums.DriversList;
import managers.PageObjectManager;
import managers.DriverManager;
import managers.ReportManager;
import stepDefinition.Hooks;

import java.util.HashMap;
import java.util.Map;

public class TestContext {


    private Map<String, Object> testContext;
    public DriversList browser;
    private DriverManager webDriverManager;
    private ReportManager reportManager;

    public TestContext(DriversList tag) {
        webDriverManager = new DriverManager(tag);
        reportManager = new ReportManager(Hooks.scenario.getName()+"_"+tag.toString());
        testContext = new HashMap<>();
        browser = tag;
    }

    public DriverManager getWebDriverManager() {
        return webDriverManager;
    }


    public ReportManager getReportManager(){
        return reportManager;
    }


//  **************************************************************************************************************//
//  ************************************  this bloc we can manage data between steps *****************************//
//  **************************************************************************************************************//

    public TestContext(){
        testContext = new HashMap<>();
    }

    public void setContext(Context key, Object value)
    {
        testContext.put(key.toString(), value);
    }

    public void setContext(String key, Object value)
    {
        testContext.put(key, value);
    }

    public Object getContext(Context key)
    {        return testContext.get(key.toString());    }

    public Boolean isContains(Context key){        return testContext.containsKey(key.toString());    }

}