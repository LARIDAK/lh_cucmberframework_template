package runners;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import stepDefinition.Hooks;


@RunWith(Cucumber.class)
@CucumberOptions(
//        features = "src\\test\\resources\\MandatoryAcceptanceTests\\Authenticate_Features\\LoginAndInterfaceChecks.feature"
        features = "src\\test\\resources\\MandatoryAcceptanceTests\\"
        ,glue={"stepDefinition"}
        ,plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"}
)
public class TestRunnerChrome {

    @BeforeClass
    public static void setBrowser() {
        Hooks.browser = "Chrome";
    }
}
