package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import stepDefinition.Hooks;


@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src\\test\\resources\\MandatoryAcceptanceTests\\MiTest\\MI_KID_Login_to_MI_and_Plan_the_session_and_Logout.feature"        , glue = {"stepDefinition"}
        , plugin = {"pretty", "html:target/cucumber"}

)
public class TestRunnerEDGE {
    @BeforeClass
    public static void setBrowser() {
        Hooks.browser = "Edge";
    }
}
