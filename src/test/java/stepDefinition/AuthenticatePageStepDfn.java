package stepDefinition;

import com.aventstack.extentreports.Status;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberContext.TestContext;
import listeners.EventReporter;
import managers.ConfigReaderManager;
import managers.ReportManager;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import pageObjects.AuthenticatePage;
import seleniumRobot.SeleniumBase;

import static enums.Context.PLATFORME;
import static enums.Context.SESSION_KEY;
import static seleniumRobot.SeleniumBase.*;

@Listeners(EventReporter.class)
public class AuthenticatePageStepDfn {

    //Script Environment variables
    WebDriver driver;
    public static WebDriver driver2;
    TestContext testContext;
    TestContext participantTestContext;

    //Report variables
    ReportManager reportManager;

    public static final String USERLOGIN = "USERLOGIN";

    //Pages Objects
    AuthenticatePage authenticatePagePresenter;
    AuthenticatePage authenticatePageParticipant;



    public AuthenticatePageStepDfn() {
        testContext = Hooks.testContext;
        driver = testContext.getWebDriverManager().getDriver();
        reportManager = testContext.getReportManager();

        authenticatePagePresenter = new AuthenticatePage(driver);

    }

    @Then("^User authenticate with a participant \"([^\"]*)\"$")
    public void

    userAuthenticateWithAParticipant(String participantLogin) throws Throwable {
        // Write code here that turns the phrase above into concrete actions


        try {
//            reportManager.createTest("Scenario : " + Hooks.scenario.getName(), "Participant : " + user.getLOGIN() + " is on authenticate page !!");
            reportManager.log(Status.PASS, "User fill the authentication form ");
        } catch (Exception e) {
            reportManager.log(Status.FAIL, "User can't fill the authentication form " + e.getMessage());
        }
    }

    @Then("^User authenticate with a presenter \"([^\"]*)\"$")
    public void userAuthenticateWithAPresenter(String presenterLogin) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^The user is disconnected$")
    public void theUserIsDisconnected() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        try {
            Assert.assertTrue(SeleniumBase.isElementPresent(driver, authenticatePagePresenter.weTextNameParticipant));
            reportManager.log(Status.PASS, "User is on the authenticate page");
        } catch (AssertionError failure) {
            reportManager.log(Status.FAIL, "User is not on the authenticate page:" + failure.getMessage());
        }
    }

    @And("^The participant get error message$")
    public void theParticipantGetErrorMessage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        try {
            SeleniumBase.isElementPresent(driver, authenticatePageParticipant.weErrorMsgLoginParticipant);
            reportManager.log(Status.PASS, "user get the error message :" + authenticatePageParticipant.weErrorMsgLoginParticipant.getText());
        } catch (Exception e) {
            reportManager.log(Status.PASS, "user don't get the error message !");
        }
    }

    @And("^The presenter get error message$")
    public void thePresenterGetErrorMessage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        try {
            Thread.sleep(1000);
            SeleniumBase.isElementPresent(driver, authenticatePagePresenter.weErrorMsgLoginPresenter);
            reportManager.log(Status.PASS, "user get the error message :" + authenticatePagePresenter.weErrorMsgLoginPresenter.getText());
        } catch (Exception e) {
            reportManager.log(Status.FAIL, "user don't get the error message !");
        }
    }

    @And("^The user click on the link and check the popup frame$")
    public void theUserClickOnTheLinkAndCheckThePopupFrame() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        try {
            SeleniumBase.isElementPresent(driver, authenticatePagePresenter.weLinkLostPassword);
            authenticatePagePresenter.weLinkLostPassword.click();
            reportManager.log(Status.PASS, "User click on reset password link !");
        } catch (Exception e) {
            reportManager.log(Status.PASS, "User can't click on reset link because of :" + e.getMessage());
        }
    }

    @And("^The user cancel the popup frame and go back to the authentication page$")
    public void theUserCancelThePopupFrameAndGoBackToTheAuthenticationPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        try {
            Assert.assertTrue(SeleniumBase.isElementPresent(driver, authenticatePagePresenter.weBtnCancelLostPwdPopup));
            reportManager.log(Status.PASS, "The popup message appear correctly !");
            authenticatePagePresenter.weBtnCancelLostPwdPopup.click();
            reportManager.log(Status.PASS, "The user cancel the lost password popup form");

        } catch (Exception e) {
            reportManager.log(Status.FAIL, e.getMessage());
        }
    }

    @And("^participant is on kid authenticate page \"([^\"]*)\"$")
    public void participantIsOnKidAuthenticatePage(String platform) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        participantTestContext = new TestContext(testContext.browser);
        driver2 = participantTestContext.getWebDriverManager().getDriver();
        authenticatePageParticipant = new AuthenticatePage(driver2);



        if (platform.equalsIgnoreCase("mikid"))
            SeleniumBase.navigateToUrl(driver2, ConfigReaderManager.getInstance().getConfigReader().getMiKidApplicationUrl());
        else
            SeleniumBase.navigateToUrl(driver2, ConfigReaderManager.getInstance().getConfigReader().getApplicationUrl());

        reportManager.log(Status.PASS, "user is on authenticate page !!");
        System.out.println("participant is on authenticate page !!");
    }

    @Given("^user is on mikid authenticate page$")
    public void userIsOnKidAuthenticatePage() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        try {
            SeleniumBase.navigateToUrl(driver2, ConfigReaderManager.getInstance().getConfigReader().getMiKidApplicationUrl());
            reportManager.log(Status.PASS, "user is on authenticate page !!");
            System.out.println("participant is on authenticate page !!");
        }catch(Exception e){
            e.printStackTrace();
            setScore(driver, "fail");
            reportManager.log(Status.FAIL, "Participant is not able to join the application url");
            driver2.quit();
        }
    }

    @And("^closing the participants connection$")
    public void closingParticipantConnection() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        SeleniumBase.setScore(driver2, "pass");
        driver2.quit();
        reportManager.log(Status.PASS, "user is on authenticate page !!");

    }

    @And("^participant joined the session with session key$")
    public void the_Participant_joined_the_session_with_session_key() throws Exception {
        try {
            if (!testContext.getContext(PLATFORME).toString().equalsIgnoreCase("mi")) {
                if (SeleniumBase.isElementPresent(driver2, authenticatePageParticipant.weTextNameParticipant)) {
                    SeleniumBase.set_Field(authenticatePageParticipant.weTextNameParticipant, "Participant");
                }
            }
            System.out.println("Session key :"+testContext.getContext(SESSION_KEY));
            authenticatePageParticipant.participantAuthenticateToken((String) testContext.getContext(SESSION_KEY));
            Thread.sleep(5000);
            setScore(driver2, "pass");
            reportManager.log(Status.PASS, "Participant is able to join with key ");
        } catch (Exception e) {
            e.printStackTrace();
            setScore(driver, "fail");
            reportManager.log(Status.FAIL, "Participant is not able to join with key ");
            driver2.quit();
        }
    }

    @And("^participant joining the session with session key after call is finished$")
    public void participantJoiningTheSessionWithSessionKeyAfterCallIsFinished() throws Exception {
        try {
            String token = testContext.getContext(SESSION_KEY).toString();
            if (!testContext.getContext(PLATFORME).toString().equalsIgnoreCase("mi")) {
                if (SeleniumBase.isElementPresent(driver2, authenticatePageParticipant.weTextNameParticipant)) {
                    SeleniumBase.set_Field(authenticatePageParticipant.weTextNameParticipant, "Participant");
                }
            }
            authenticatePageParticipant.participantAuthenticateToken(token);
            setScore(driver2, "pass");
            driver2.quit();
            reportManager.log(Status.PASS, "Participant authenticate with a session Key");
        } catch (Exception e) {
            e.printStackTrace();
            setScore(driver2, "fail");
            reportManager.log(Status.FAIL, "Participant is not able to join with key ");
            driver2.quit();
        }
    }

    //*******************************************************************//
    //************************ MI Authenticate **************************//
    //*******************************************************************//

    @And("^Check if we are in the participant home page$")
    public void checkIfWeAreInTheParticipantHomePage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        try {

            Thread.sleep(5000);
            reportManager.log(Status.PASS, "Participant is in participant home page ");

        } catch (Exception e) {
            e.printStackTrace();
            setScore(driver2, "fail");
            reportManager.log(Status.FAIL, "Participant is not able to join with key ");
            driver2.quit();
        }
    }

    @When("^participant crashes his browser$")
    public void participantCrashesHisBrowser() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver2.quit();
    }

    @Then("^Change application languages \"([^\"]*)\"$")
    public void changeApplicationLanguages(String participantLogin) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }
}
