package stepDefinition;

import com.aventstack.extentreports.ExtentTest;
import com.cucumber.listener.Reporter;
import com.google.common.io.Files;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumberContext.TestContext;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import runners.TestRunnerChrome;
import runners.TestRunnerEDGE;


import java.io.File;
import java.io.IOException;

import static enums.DriversList.*;

public class Hooks {

    public static TestContext testContext;

    public static String browser = "";
    public static Scenario scenario;

    @Before(order = 0)
    public void beforeScenarios(Scenario scenario) {
        System.out.println("Browser name :" + browser);
        this.scenario = scenario;

        switch (browser) {
            case "FF":
                testContext = new TestContext(FIREFOX);
                break;
            case "Chrome":
                testContext = new TestContext(CHROME);
                break;
            case "IE":
                testContext = new TestContext(INTERNETEXPLORER);
                break;
            case "Edge":
                testContext = new TestContext(EDGE);
                break;
            case "Safari":
                testContext = new TestContext(SAFARI);
                break;

        }
        System.out.println("i'm in before 0");
    }


    @Before("@FF")
    public void setUpFirefox() {
        testContext = new TestContext(FIREFOX);
        System.out.println("FF");
    }

    @Before("@Chrome")
    public void setUpChrome() {
        testContext = new TestContext(CHROME);
        System.out.println("Chrome");
    }

    @Before("@IE")
    public void setUpIE() {
        testContext = new TestContext(INTERNETEXPLORER);
        System.out.println("IE");
    }

    @Before("@Edge")
    public void setUpEdge() {
        testContext = new TestContext(EDGE);
        System.out.println("Edge");
    }

    @Before("@Safari")
    public void setUpSafari() {
        testContext = new TestContext(SAFARI);
        System.out.println("Safari");
    }


    /************************************************************
     *
     *
     * @param scenario
     */
    @After(order = 1)
    public void afterScenario(Scenario scenario) {
        if (scenario.isFailed()) {
            String screenshotName = scenario.getName().replaceAll(" ", "_");
            try {
                //This takes a screenshot from the driver at save it to the specified location
                File sourcePath = ((TakesScreenshot) testContext.getWebDriverManager().getDriver()).getScreenshotAs(OutputType.FILE);

                //Building up the destination path for the screenshot to save
                //Also make sure to create a folder 'screenshots' with in the cucumber-report folder
                File destinationPath = new File(System.getProperty("user.dir") + "/target/cucumber-reports/screenshots/" + screenshotName + ".png");

                //Copy taken screenshot from source location to destination location
                Files.copy(sourcePath, destinationPath);

                //This attach the specified screenshot to the test
                Reporter.addScreenCaptureFromPath(destinationPath.toString());
            } catch (IOException e) {
            }
        }
    }

    @After(order = 0)
    public void AfterSteps() {
        System.out.println("************************** After Test !!");
        testContext.getReportManager().flush();
        testContext.getWebDriverManager().closeDriver();

        if(AuthenticatePageStepDfn.driver2 != null)
            AuthenticatePageStepDfn.driver2.quit();

    }


}
